$(document).on('ready',function(){

  var headers = [];
  $('#content .markdown h2').each(function(){
    var header = $(this).text();
    $(this).prepend('<div id="section-' + releaseHeaderToAnchor(header) + '"></div>');
    headers.push(header);
  });
  
  var $notes_list = $('#content .notes-list');
  $.each(headers, function(i, header) {
    $notes_list.append($('<li><a data-target="' + releaseHeaderToAnchor(header) + '" class="jump">' + header + '</a></li>'))
  });

});

function releaseHeaderToAnchor(header) {
  var anchor = header.toLowerCase();
  anchor = anchor.replace(/ /g, "-");
  anchor = anchor.replace(/[^a-z0-9\-]/g, "");
  return anchor;
}


$(document).ready(function() {
    var offset = 250;
    var duration = 200;
    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(duration);
        } else {
            $('.back-to-top').fadeOut(duration);
        }
    });
	
    $('.back-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, duration);
        return false;
    })

    
});
;
