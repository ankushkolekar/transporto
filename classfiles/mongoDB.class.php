<?php
/**
* Using this class we can easily access any information related to ispice database
*
* @see 	mongoDBCls class
* @author Shashikant Sharma
*/
class mongoDBCls{
	/**
	* function __construct calls parent class constructor.
	*/
	function __construct(){}
	
	/**
	* @see function funGETDatabaseOBJ 
	* function funGETDatabaseOBJ Connects to MONGODB and creates its database object
	*/
	public function funGETDatabaseOBJ(){
		ini_set('display_errors', 1); 
			error_reporting(-1); error_reporting(E_ALL); 
		try{
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				$mongoObj = new MongoClient("mongodb://localhost:27017");
			}
			else{ 
				$mongoObj = new MongoClient(); 
			}
			
			$dbObj = $mongoObj->Transporto;
			return $dbObj;
		}catch(Exception $e){ echo $e->getMessage();}
	}
	
}

?>