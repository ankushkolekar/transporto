'use strict';

;(function (root, $, uship) {
    'use strict';

    var shouldTrack = true;

    function trackEvent(eventName, data, callback) {
        if (!hasMixpanel()) {
            return proxy('event').apply(null, arguments);
        }

        if (!shouldTrack) return;

        mixpanel.track(eventName, data, callback);
    }

    function trackEventAsync(eventName, data, resolveWithData) {
        var dfd = $.Deferred(),
            promise = dfd.promise();

        if (!hasMixpanel()) {
            dfd.resolve(resolveWithData);
        } else {
            if (!shouldTrack) return;

            mixpanel.track(eventName, data, function () {
                dfd.resolve(resolveWithData);
            });
        }

        return promise;
    }

    function registerProperties(props) {
        if (!hasMixpanel()) {
            return proxy('register').apply(null, arguments);
        }
        if (!shouldTrack) return;

        mixpanel.register(props);
    }

    function registerOnce(props) {
        if (!hasMixpanel()) {
            return proxy('registerOnce').apply(null, arguments);
        }
        if (!shouldTrack) return;

        mixpanel.register_once(props);
    }

    function identifyUser(id) {
        if (!hasMixpanel()) {
            return proxy('identifyUser').apply(null, arguments);
        }
        if (!shouldTrack) return;

        mixpanel.identify(id);
    }

    function trackClick(linkSelector, eventName, data, callback) {
        if (!hasMixpanel()) {
            return proxy('linkClick').apply(null, arguments);
        }
        if (!shouldTrack) return;

        mixpanel.track_links(linkSelector, eventName, data, callback);
    }

    function hasMixpanel() {
        return typeof root.mixpanel !== 'undefined';
    }

    function proxy(name, fn) {
        if (arguments.length === 2) {
            proxy[name] = uship.utils.asCallable(fn);
            return;
        }

        return proxy[name] || uship.utils.noop;
    }

    // Proxies

    function consoleLogEvent(eventName, data, callback) {
        console.log(eventName, data);
        uship.utils.asCallable(callback)();
    }

    function consoleLogLinkClick(linkSelector, eventName, data, callback) {
        if (!$) return;
        $(linkSelector).on('click', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            trackEvent(eventName, data, function (response) {
                uship.utils.asCallable(callback).call(null, response);
                console.log('Navigating to ', e.target.getAttribute('href'));
                //simulateHyperlinkClick(e);
            });
        });
    }

    function simulateHyperlinkClick(e) {
        var link = $(e.target),
            href = link.attr('href'),
            target = link.attr('target');

        if ($.trim(target).length || event.which === 2) {
            root.open(href, target);
        } else {
            root.location = href;
        }
    }

    // This function takes in a value between 0 and 1 to throttle mixpanel events on a page. It works on a per page not a per event basis. So call on page load.
    function setThrottle(throttlePercent) {

        if (Math.random() < throttlePercent) {
            shouldTrack = true;
        } else {
            shouldTrack = false;
        }

        return true;
    }

    uship.namespace('track').extend({
        event: trackEvent,
        linkClick: trackClick,
        async: trackEventAsync,
        register: registerProperties,
        registerOnce: registerOnce,
        identify: identifyUser,
        proxy: proxy,
        proxies: {
            consoleLogEvent: consoleLogEvent,
            consoleLogLinkClick: consoleLogLinkClick
        },
        throttle: setThrottle
    });
})(window, jQuery, uship);