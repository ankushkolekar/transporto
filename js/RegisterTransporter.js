
function UserType(pUserTypeId) {


     this.userTypeId=pUserTypeId;
     this.name=null;
     this.description=null;
       
}
function User(pEmailAddress, pPassword, pFirstName, pLastName,pMobile,pDateOfBirth,pGender,pAlias,pUserType) {
 
     
     this.userId=null;
     this.userType=pUserType;
     this.emailAddress=pEmailAddress;
     this.firstName=pFirstName;
     this.lastName=pLastName;
     this.mobile=pMobile;
     this.password=pPassword;
     this.otp=0;
     this.otpverified=0;
     this.otpgeneratedAt=null;
     this.otpverifiedAt=null;
     this.photoUrl=null;
     this.gender=pGender;
     this.varOfBirth=null;
     this.enabled=true;
     this.alias=pAlias;
     this.loggedIn=false;
     this.gcmId=null;
}

function City(cityId){
this.cityId=cityId;
this.name=null;
this.code=null;
this.state=null;
this.locationId=null;

}


function Country(countryId){
this.countryId=countryId;
this.name=null;
this.code=null;
this.currencyId=null;
this.locationId=null;

}


function State(stateId){
this.stateId=stateId;
this.name=null;
this.code=null;
this.country=null;
this.locationId=null;

}

function TransporterType (pTransporterTypeId) {


     this.TransporterTypeId=pTransporterTypeId;
     this.description="";
  
}


function BusinessCategory(pBusinessCategoryId) {


     this.businessCategoryId=pBusinessCategoryId;
     this.businessCategory="";
     this.description="";
}

function Transporter(user,name,address,pincode){
this.user=user;
this.name=name;
this.address=address;
this.pinCode=pincode;
this.city=null;
this.state=null;
this.country=null;
this.financialPeriod=null;
this.discoverabilityEnabled=null;
this.transporterId=null;

}
function FinancialPeriod(financialPeriodId){
this.financialPeriodId=financialPeriodId;
this.code=null;
this.startDate=null;
this.endDate=null;
this.closed=null;
this.remarks=null;

}

function TransporterRegisterParams (pUser,pTransporter){
	this.user=pUser;
	this.transporter=pTransporter;
	this.transporter["@class"]="com.dliverapp.datamodel.Transporter";
}



function buildRegisterTransporterParams(pEmailAddress, pPassword, pFirstName, pLastName,pMobile,pDateOfBirth,pGender,pAlias,pName,pAddress,pPincode){
var pUserType=new UserType('ROLE_Transporter');
var pUser=new User(pEmailAddress, pPassword, pFirstName, pLastName,pMobile,pDateOfBirth,pGender,pAlias,pUserType);
pBusinessCategory=new BusinessCategory('GENERAL');
pTransporterType=new TransporterType(null);
var pTransporter=new Transporter(pUser,pName,pAddress,pPincode);
var city=new City('MUM');
var state=new State('MAH');
var country=new Country('IN');
var financialPeriod=new FinancialPeriod('2015-16');
pTransporter.financialPeriod=financialPeriod;
pTransporter.city=city;
pTransporter.state=state;
pTransporter.country=country;
this.params=new TransporterRegisterParams(pUser,pTransporter);
console.log(JSON.stringify(this.params))
return JSON.stringify(this.params);
}


function registerTransporter(pEmailAddress, pPassword, pFirstName, pLastName,pMobile,pDateOfBirth,pGender,pAlias,pName,pAddress,pPincode){
	
var data=buildRegisterTransporterParams(pEmailAddress, pPassword, pFirstName, pLastName,pMobile,pDateOfBirth,pGender,pAlias,pName,pAddress,pPincode);
  $.ajax
      ({
        type: "POST",
        url: "http://59.99.229.207:8080/Transporto-Server-InterCity-I/rest/transporter/register",
        dataType: 'json',
        crossDomain: true,
        async: false,
        data: data,
        contentType: "application/json",
        success: function (response){
			
						console.log(response);
								if(response.responseMessage=='OK'){
										alert("SuccessFully Registered As a Transporter");
									}else{
										alert(response.responseCode+"::::"+response.responseMessage);
									}
								
			},
        error:function(x,t,m){
            alert("Invali Registration");
             var err = eval("(" + x.responseText + ")");
			 
        }
    });  
    };





