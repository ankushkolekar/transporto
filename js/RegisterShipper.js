
function UserType(pUserTypeId) {


     this.userTypeId=pUserTypeId;
     this.name=null;
     this.description=null;
       
}
function User(pEmailAddress, pPassword, pFirstName, pLastName,pMobile,pDateOfBirth,pGender,pAlias,pUserType) {
 
     
     this.userId=null;
     this.userType=pUserType;
     this.emailAddress=pEmailAddress;
     this.firstName=pFirstName;
     this.lastName=pLastName;
     this.mobile=pMobile;
     this.password=pPassword;
     this.otp=0;
     this.otpverified=0;
     this.otpgeneratedAt=null;
     this.otpverifiedAt=null;
     this.photoUrl=null;
     this.gender=pGender;
     this.varOfBirth=null;
     this.enabled=true;
     this.alias=pAlias;
     this.loggedIn=false;
     this.gcmId=null;
}



function ShipperType (pShipperTypeId) {


     this.shipperTypeId=pShipperTypeId;
     this.description="";
  
}


function BusinessCategory(pBusinessCategoryId) {


     this.businessCategoryId=pBusinessCategoryId;
     this.businessCategory="";
     this.description="";
}

function Shipper(pBusinessCategory,pShipperType,pUser){

     
     this.shipperId=null;
     this.businessCategory =new BusinessCategory('GENERAL');
     this.shipperType=new ShipperType();
     this.user=pUser;
     this.appRating=0;
     this.defaultPickupAddressId=null;
     this.defaultDropoffAddressId=null;
}


function ShipperRegisterParams (pUser,pShipper){
	this.user=pUser;
	this.shipper=pShipper;
	this.shipper["@class"]="com.dliverapp.datamodel.Shipper";
}



function buildRegisterShipperParams(pEmailAddress, pPassword, pFirstName, pLastName,pMobile,pDateOfBirth,pGender,pAlias){
var pUserType=new UserType('ROLE_SHIPPER');
var pUser=new User(pEmailAddress, pPassword, pFirstName, pLastName,pMobile,pDateOfBirth,pGender,pAlias,pUserType);
pBusinessCategory=new BusinessCategory('GENERAL');
pShipperType=new ShipperType(null);
var pShipper=new Shipper(pBusinessCategory,pShipperType,pUser);
this.params=new ShipperRegisterParams(pUser,pShipper);
console.log(JSON.stringify(this.params))
return JSON.stringify(this.params);
}


function registerShipper(pEmailAddress, pPassword, pFirstName, pLastName,pMobile,pDateOfBirth,pGender,pAlias){
	
var data=buildRegisterShipperParams(pEmailAddress, pPassword, pFirstName, pLastName,pMobile,pDateOfBirth,pGender,pAlias);
  $.ajax
      ({
        type: "POST",
        url: "http://59.99.229.207:8080/Transporto-Server-InterCity-I/rest/shipper/register",
        dataType: 'json',
        crossDomain: true,
        async: false,
        data: data,
        contentType: "application/json",
        success: function (response){
			
						console.log(response);
								if(response.responseMessage=='OK'){
										alert("SuccessFully Registered");
									}else{
										alert(response.responseCode+"::::"+response.responseMessage);
									}
								
			},
        error:function(x,t,m){
            alert("Invali Registration");
             var err = eval("(" + x.responseText + ")");
			 
        }
    });  
    };





