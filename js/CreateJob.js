function buildJob(quote,shipper,cargoCategoryId,vehicleCategoryId,unitOfDimensionId,unitOfWeightId,unitOfVolumeId,pickupAddress,dropoffAddress,length,width,height,weight,volume,alias,validUpto,pickdate,desc,paymentType){
var job=new Job();
job.cargoCategory=new CargoCategory(cargoCategoryId);
job.vehicleCategory=new VehicleCategory(vehicleCategoryId);
job.unitOfDimension=new UnitOfDimension(unitOfDimensionId);
job.unitOfVolume=new UnitOfVolume(unitOfVolumeId);
job.unitOfWeight=new UnitOfWeight(unitOfWeightId);
job.paymentType =new PaymentType(paymentType);
job.shipperAddressByPickupAddressId=pickupAddress;
job.shipperAddressByDropoffAddressId=dropoffAddress;
job.shipperQuote = quote;
job.closedVehicle = false;
job.systemSelectsTransporter = false;
job.shipper = shipper;
job.length=length;
job.volume = volume;
job.breadth=width;
job.height=height;
job.weight=weight;
job.alias=alias;
job.validUpto = validUpto;
job.serviceDate = pickdate;
job.description = desc;
return job;
}

function Job(jobId){
this ["@class"] = "com.dliverapp.datamodel.job";	
this.jobId=jobId;
this.date=null;
this.shipper=null;
this.cargoCategory=null;
this.imageUrl=null;
this.description=null;
this.serviceDate=null;
this.length=null;
this.breadth=null;
this.height=null;
this.weight=null;
this.volume=null;
this.unitOfDimension=null;
this.unitOfWeight=null;
this.unitOfVolume=null;
this.vehicleCategory=null;
this.shipperQuote=null;
this.validUpto=null;
this.confirmedOn=null;
this.alias=null;
this.shipperAddressByPickupAddressId=null;
this.shipperAddressByDropoffAddressId=null;
this.jobStatus=null;
this.negotiationStatus=null;
this.transporterId=null;
this.transporterQuoteId=null;
this.transporterQuote=null;
this.commission=null;
this.paymentType= null;
this.closedVehicle=null;
this.shipperRating=null;
this.transporterRating=null;
this.riderRating=null;
this.vehicle=null;
this.rider=null;
this.riderAssigned=null;
this.paymentCollected=null;
this.financialPeriod= new FinancialPeriod('2015-16');
this.serialNumber=null;
this.pickupDate=null;
this.dropoffDate=null;
this.suggestedPrice=null;
this.pricingPlan=null;
this.systemSelectsTransporter=null;
this.lastModified=null;
this.distance=null;
}

function ShipperAddress(shipper,landmark,city,latitude,longitude,unit,apartment,street,locality,state,country,pincode,name,phone){
this ["@class"] = "com.dliverapp.datamodel.ShipperAddress";	
this.shipperAddressId=null;
this.shipper=shipper;
this.landmark=landmark;
this.city=city;
this.latitude=latitude;
this.longitude=longitude;
this._Default=null;
this.unit=unit;
this.apartment=apartment;
this.street=street;
this.locality=locality;
this.state=state;
this.country=country;
this.pinCode=pincode;
this.name=name;
this.phone=phone;
this.pickupOrDropoff=null;

}
function City(cityId){
this ["@class"] = "com.dliverapp.datamodel.City";	
this.cityId=cityId;
this.name=null;
this.code=null;
this.state=null;
this.locationId=null;

}


function Country(countryId){
this ["@class"] = "com.dliverapp.datamodel.country";	
this.countryId=countryId;
this.name=null;
this.code=null;
this.currencyId=null;
this.locationId=null;

}

function buildShipperAddressPickup(shipper,city,latitude,longitude,address,state,country,pincode,name){
var shipperAddressId=null;
var shipper=shipper;
var landmark= null;
var city= city;
var latitude=latitude;
var longitude=longitude;
var _Default=null;
var unita= address;
var apartment= null;
var street= null;
var locality= null;
var state=state;
var country=country;
var pinCode=pincode;
var name= name;
var phone= '345454654';
var pickupOrDropoff=null;

var pickup=new ShipperAddress(shipper,landmark,city,latitude,longitude,unita,apartment,street,locality,state,country,pincode,name,phone);
return pickup;
}



function buildShipperAddressDropoff(shipper,city,latitude,longitude,address,state,country,pincode,name){
var shipperAddressId=null;
var shipper=shipper;
var landmark= null;
var city=city;
var latitude=latitude;
var longitude=longitude;
var _Default=null;
var unita= address;
var apartment= null;
var street= null;
var locality= null;
var state=state;
var country=country;
var pinCode=pincode;
var name= name;
var phone= '324354354';
var pickupOrDropoff=null;

var dropoff=new ShipperAddress(shipper,landmark,city,latitude,longitude,unita,apartment,street,locality,state,country,pincode,name,phone);
return dropoff;
}
function CargoCategory(cargoCategoryId){
this ["@class"] = "com.dliverapp.datamodel.CargoCategory";	
this.cargoCategoryId=cargoCategoryId;
this.cargoCategory=null;
this.code=null;
this.description=null;

}
function VehicleCategory(vehicleCategoryId){
this ["@class"] = "com.dliverapp.datamodel.VehicleCategory";		
this.vehicleCategoryId=vehicleCategoryId;
this.vehicleCategory=null;
this.code=null;
this.length=null;
this.breadth=null;
this.height=null;
this.weight=null;
this.volume=null;
this.unitOfDimension=null;
this.unitOfVolume=null;
this.unitOfWeight=null;

}
function UnitOfDimension(unitId){
this ["@class"] = "com.dliverapp.datamodel.UnitOfDimension";		
this.unitId=unitId;
this.code=null;
this.description=null;

}


function UnitOfVolume(unitId){
this ["@class"] = "com.dliverapp.datamodel.UnitOfVolume";	
this.unitId=unitId;
this.code=null;
this.description=null;

}


function UnitOfWeight(unitId){
this ["@class"] = "com.dliverapp.datamodel.UnitOfWeight";	
this.unitId=unitId;
this.code=null;
this.description=null;

}
function State(stateId){
this ["@class"] = "com.dliverapp.datamodel.State";	
this.stateId=stateId;
this.name=null;
this.code=null;
this.country=null;
this.locationId=null;

}
function Shipper(shipperId){
this ["@class"] = "com.dliverapp.datamodel.Shipper";		
this.user=null;
this.shipperType=null;
this.appRating=null;
this.defaultPickupAddressId=null;
this.defaultDropoffAddressId=null;
this.businessCategory=null;
this.shipperId=shipperId;

}
function FinancialPeriod(financialPeriodId){
this ["@class"] = "com.dliverapp.datamodel.FinancialPeriod";	
this.financialPeriodId=financialPeriodId;
this.code=null;
this.startDate=null;
this.endDate=null;
this.closed=null;
this.remarks=null;

}
function PaymentType(paymentTypeId){
this ["@class"] = "com.dliverapp.datamodel.PaymentType";	
this.paymentTypeId=paymentTypeId;
this.paymentType=null;
this.description=null;

}
