function Transporter(transporterId){
this.transporterId=transporterId=null;


     this["@class"]="com.dliverapp.datamodel.Transporter";
     this.city=null;
     this.country=null;
     this.financialPeriod=null;
     this.state=null;
     this.user=null;
     this.name=null;
     this.address=null;
     this.pinCode=null;
     this.discoverabilityEnable=null;
     this.rating=null;

}



function Job(jobId){
this.jobId=jobId=null;


     this["@class"]="com.dliverapp.datamodel.Job";
     this.cargoCategory=null;
     this.financialPeriod=null;
     this.jobStatus=null;
     this.negotiationStatus=null;
     this.paymentType=null;
     this.rider=null;
     this.shipper=null;
     this.shipperAddressByDropoffAddressId=null;
     this.shipperAddressByPickupAddressId=null;
     this.unitOfDimension=null;
     this.unitOfVolume=null;
     this.unitOfWeight=null;
     this.vehicle=null;
     this.vehicleCategory=null;
     this.date=null;
     this.lastModified=null;
     this.imageUrl=null;
     this.description=null;
     this.serviceDate=null;
     this.length=null;
     this.breadth=null;
     this.height=null;
     this.weight=null;
     this.volume=null;
     this.shipperQuote=null;
     this.validUpto=null;
     this.confirmedOn=null;
     this.pickupDate=null;
     this.dropoffDate=null;
     this.alias=null;
     this.transporterId=null;
     this.transporterQuoteId=null;
     this.transporterQuote=null;
     this.commission=null;
     this.closedVehicle=null;
     this.shipperRating=null;
     this.transporterRating=null;
     this.riderRating=null;
     this.riderAssigned=null;
     this.paymentCollected=null;
     this.serialNumber=null;
     this.pricingPlan=null;
     this.systemSelectsTransporter=null;
     this.suggestedPrice=null;
     this.distance=null;
}




function Quote(quote,remarks,validUpto,jobId,transporterId,date) {

    this["@class"]="com.dliverapp.datamodel.Quote";
    this.quoteId=null;
    this.job=new Job(jobId);
    this.transporter=new Transporter(transporterId);
    this.date=date;
    this.quote=quote;
    this.remarks=remarks;
    this.accepted=false;
    this.validUpto=validUpto;
}
